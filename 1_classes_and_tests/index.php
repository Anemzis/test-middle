<?php

require_once __DIR__ . '/vendor/autoload.php';

define('APPLICATION_NAME', 'Drive Test');
define('CREDENTIALS_PATH', __DIR__ . '/credentials/google-drive-credentials-http.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secrets.json');
define('SCOPES', Google_Service_Drive::DRIVE_METADATA_READONLY);

use App\GoogleDrive\CredentialsRepositoryFile;
use App\GoogleDrive\GoogleClientFactory;
use App\GoogleDrive\GoogleDriveWrapper;

$credentialsRepository = new CredentialsRepositoryFile(CREDENTIALS_PATH);
$factory = new GoogleClientFactory(APPLICATION_NAME,
    SCOPES,
    CLIENT_SECRET_PATH,
    $credentialsRepository);

$client = $factory->create();
$drive = new GoogleDriveWrapper($client);
$drive->listLastMonth();