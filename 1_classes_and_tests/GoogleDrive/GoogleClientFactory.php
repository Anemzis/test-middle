<?php

namespace App\GoogleDrive;

use Google_Client;

class GoogleClientFactory
{
    private $applicationName;
    private $scopes;
    private $clientSecretPath;
    private $credentialsRepository;

    public function __construct(
        string $applicationName,
        string $scopes,
        string $clientSecretPath,
        CredentialsRepository $credentialsRepository
    ) {
        $this->applicationName = $applicationName;
        $this->scopes = $scopes;
        $this->clientSecretPath = $clientSecretPath;
        $this->credentialsRepository = $credentialsRepository;
    }

    public function create(string $authCode = ''): Google_Client
    {
        $client = new Google_Client();
        $client->setApplicationName($this->applicationName);
        $client->setScopes($this->scopes);
        $client->setAuthConfigFile($this->clientSecretPath);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $accessToken = $this->credentialsRepository->loadAccessToken();
        if (empty($accessToken)) {
            if (empty($authCode)) {
                $authUrl = $client->createAuthUrl();
                $authCode = $this->askForAuthCode($authUrl);
            }
            $accessToken = $client->authenticate($authCode);
            $this->credentialsRepository->storeAccessToken(json_encode($accessToken));
        }
        $client->setAccessToken($accessToken);

        if ($client->isAccessTokenExpired()) {
            $client->refreshToken($client->getRefreshToken());
            $this->credentialsRepository->storeAccessToken(json_encode($client->getAccessToken()));
        }
        $client->setDefer(true);
        if (!$client->getAccessToken()) {
            throw new PermissionException('No access token');
        }
        return $client;
    }

    public function askForAuthCode(string $authUrl)
    {
        header('location:' . $authUrl);
        die();
    }
}