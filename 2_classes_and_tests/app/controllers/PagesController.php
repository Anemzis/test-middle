<?php

namespace App\Controllers;

use App\Models\Page;

class PagesController
{
    /**
     * get all pages data
     */
    public function home()
    {
        $pages = Page::selectAll();
        echo json_encode([
            'code' => '404',
            'pages' => $pages
        ]);
    }

    /**
     * @param $data
     * get data from one page
     */
    public function pageFilter($data)
    {
        header('Content-type: application/json');
        echo json_encode([
            'code' => 200,
            'data' => $data
        ]);
    }

    /**
     * @param $message
     * error page
     */
    public function errorPage($message)
    {
        header('Content-type: application/json');
        echo json_encode([
            'code' => 404,
            'message' => $message
        ]);
    }
} 