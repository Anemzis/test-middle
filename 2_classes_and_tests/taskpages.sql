-- Host: 127.0.0.1    Database: taskpages
--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `friendly` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
INSERT INTO `pages` VALUES (1,'page1','Title 1','Description 1'),(2,'page2','Title 2','Description 2'),(3,'page3','Title 3','Description 3');
UNLOCK TABLES;
